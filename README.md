# JDK 7 Update 80 64位 Windows 版本下载

## 资源文件介绍

本仓库提供了一个资源文件的下载，文件名为 `jdk-7u80-windows-x64.zip`。该文件是适用于Windows操作系统的64位版本的JDK 7 Update 80。

## 文件说明

- **文件名**: `jdk-7u80-windows-x64.zip`
- **版本**: JDK 7 Update 80
- **适用系统**: Windows 64位

## 使用说明

1. 下载 `jdk-7u80-windows-x64.zip` 文件。
2. 解压缩文件到你希望安装JDK的目录。
3. 配置环境变量，将JDK的`bin`目录添加到系统的`PATH`中。
4. 验证安装是否成功，打开命令行工具并输入 `java -version`，确认输出显示为 `java version "1.7.0_80"`。

## 注意事项

- 请确保你的操作系统是64位的Windows系统。
- 在配置环境变量时，请注意路径的正确性。

## 其他信息

如果你有任何问题或需要进一步的帮助，请在仓库中提交问题。